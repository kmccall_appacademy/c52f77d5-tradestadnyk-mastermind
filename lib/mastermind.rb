class Code
  PEGS = {
      "R"=>"Red","G"=>"Green","B"=>"Blue","Y"=>"Yellow","O"=>"Orange","P"=>"Purple"
    }

  attr_reader :pegs

  def self.parse(code_string)
    chars = code_string.chars.map(&:upcase)
    raise "ArgumentError" unless chars - PEGS.keys == []
    pegs = chars.map { |char| PEGS[char] }
    self.new(pegs)
  end

  def self.random
    pegs = []
    4.times { pegs << PEGS.values.sample }
    self.new(pegs)
  end

  def initialize(pegs)
    @pegs = pegs
  end

  def [](idx)
    @pegs[idx]
  end

  def exact_matches(other)
    matches = 0
    4.times { |idx| matches += 1 if @pegs[idx] == other.pegs[idx] }
    matches
  end

  def near_matches(other)
    matches = 0
    PEGS.values.each do |color|
      matches += [@pegs.count(color), other.pegs.count(color)].min
    end
    matches - exact_matches(other)
  end

  def ==(other)
    return false unless other.class == Code
    @pegs == other.pegs
  end

  def to_s
    "[@pegs.join(', ')]"
  end

end

class Game
  attr_reader :secret_code

  def  initialize(code = nil)
    @secret_code = code || Code.random
  end

  def play
    10.times do |turn|
      code = get_guess
      break if won?
      display_matches
    end
    conclude
  end

  def won?
    @secret_code == @guess
  end

  def get_guess
    print "Enter a guess (ex. 'BGRP')"
    @guess = Code.parse(gets.chomp)
  end

  def conclude
    if @guess == @secret_code
      puts "Congratulations! You win. The code was #{@secret_code}"
    else
      puts "You lost, the code was #{@secret_code}"
    end
  end

  def display_matches
    puts "near matches: #{@secret_code.near_matches}"
    puts "exact matches: #{@secret_code.exact_matches}"
  end
end

if __FILE__ == $PROGRAM_NAME
  game = Game.new(Code.parse("bbbb"))
  game.play
end
